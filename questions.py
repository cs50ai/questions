import nltk
import sys
import os
from string import punctuation
from itertools import chain
from math import log

FILE_MATCHES = 1
SENTENCE_MATCHES = 1


def main():

    # Check command-line arguments
    if len(sys.argv) != 2:
        sys.exit("Usage: python questions.py corpus")

    # Calculate IDF values across files
    files = load_files(sys.argv[1])
    file_words = {
        filename: tokenize(files[filename])
        for filename in files
    }
    file_idfs = compute_idfs(file_words)

    # Prompt user for query
    query = set(tokenize(input("Query: ")))

    # Determine top file matches according to TF-IDF
    filenames = top_files(query, file_words, file_idfs, n=FILE_MATCHES)

    # Extract sentences from top files
    sentences = dict()
    for filename in filenames:
        for passage in files[filename].split("\n"):
            for sentence in nltk.sent_tokenize(passage):
                tokens = tokenize(sentence)
                if tokens:
                    sentences[sentence] = tokens

    # Compute IDF values across sentences
    idfs = compute_idfs(sentences)

    # Determine top sentence matches
    matches = top_sentences(query, sentences, idfs, n=SENTENCE_MATCHES)
    for match in matches:
        print(match)


def extension(filename):
    """
    Return the extension of the file.
    """
    return os.path.splitext(filename)[1][1:]


def read_file(filename):
    """
    Return the content of the files as string.
    """
    with open(filename, "r") as file:
        return file.read()


def load_files(directory):
    """
    Given a directory name, return a dictionary mapping the filename of each
    `.txt` file inside that directory to the file's contents as a string.
    """
    files = [os.path.join(directory, file) for file in os.listdir(directory) if extension(file) == "txt"]
    return {os.path.basename(path): read_file(path) for path in files}


def tokenize(document):
    """
    Given a document (represented as a string), return a list of all of the
    words in that document, in order.

    Process document by coverting all words to lowercase, and removing any
    punctuation or English stopwords.
    """
    words = nltk.word_tokenize(document)
    words = [word.lower() for word in words if
        word not in punctuation and
        word not in nltk.corpus.stopwords.words("english")
    ]

    return words


def compute_idf(documents, word):
    """
    Given a dictionary of `documents` that maps names of documents to a list
    of words, return the idf of `word`.
    """
    appearances = sum(word in document for document in documents.values())
    return log(len(documents) / appearances)


def compute_idfs(documents):
    """
    Given a dictionary of `documents` that maps names of documents to a list
    of words, return a dictionary that maps words to their IDF values.

    Any word that appears in at least one of the documents should be in the
    resulting dictionary.
    """
    words = set(chain(*documents.values()))
    idfs = {word: compute_idf(documents, word) for word in words}
    return idfs


def tfidf_score(query, file, idfs):
    """
    Given a `query` (a set of words), `file` (a list of a file's words),
    and `idfs` (a dictionary mapping words to their IDF values), return
    the sum of the tf-idf scores for that file.
    """
    return sum(file.count(word) * idfs[word] for word in query if word in idfs)


def top_files(query, files, idfs, n):
    """
    Given a `query` (a set of words), `files` (a dictionary mapping names of
    files to a list of their words), and `idfs` (a dictionary mapping words
    to their IDF values), return a list of the filenames of the the `n` top
    files that match the query, ranked according to tf-idf.
    """
    tfidf_scores = {filename: tfidf_score(query, files[filename], idfs) for filename in files}
    sorted_files = sorted(files.keys(), key=lambda filename: tfidf_scores[filename], reverse=True)
    return sorted_files[:n]


def sentence_score(query, sentence_words, idfs):
    """
    Given a `query` (a set of words), `sentence_words` (a list of sentence's words),
    and `idfs` (a dictionary mapping words to their IDF values), return the sentence score,
    a tuple with "matching word measure" (sum of IDF values of words in the query that
    appears on sentence) and "query term density" (the proportion of words in the sentence
    that are also words in the query).
    """
    matching_word_measure = sum(idfs[word] for word in query if word in sentence_words)
    query_term_density = sum(word in query for word in sentence_words) / len(sentence_words)
    print(query_term_density)
    return matching_word_measure, query_term_density


def top_sentences(query, sentences, idfs, n):
    """
    Given a `query` (a set of words), `sentences` (a dictionary mapping
    sentences to a list of their words), and `idfs` (a dictionary mapping words
    to their IDF values), return a list of the `n` top sentences that match
    the query, ranked according to idf. If there are ties, preference should
    be given to sentences that have a higher query term density.
    """
    scores = {sentence: sentence_score(query, sentences[sentence], idfs) for sentence in sentences}
    sorted_sentences = sorted(sentences.keys(), key=lambda sentence: scores[sentence], reverse=True)
    return sorted_sentences[:n]


if __name__ == "__main__":
    main()
